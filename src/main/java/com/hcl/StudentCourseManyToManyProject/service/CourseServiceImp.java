package com.hcl.StudentCourseManyToManyProject.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.StudentCourseManyToManyProject.model.Course;
import com.hcl.StudentCourseManyToManyProject.repository.CourseRepository;
import com.hcl.StudentCourseManyToManyProject.repository.StudentRepository;

@Service
public class CourseServiceImp implements CourseService {

	@Autowired
	CourseRepository courseRepository;

	@Override
	public Course createCourse(Course course) {
		return courseRepository.save(course);
	}

	@Override
	public Course updateCourse(Course course) {
		return courseRepository.save(course);
	}

	@Override
	public Course getCourse(int courseId) {
		return courseRepository.getOne(courseId);
	}

	@Override
	public void deleteCourse(int courseId) {
		courseRepository.deleteById(courseId);

	}

	@Override
	public List<Course> getCourse() {
		return courseRepository.findAll();
	}

}
