package com.hcl.StudentCourseManyToManyProject.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.StudentCourseManyToManyProject.model.Student;
import com.hcl.StudentCourseManyToManyProject.repository.StudentRepository;

@Service
public class StudentServiceImp implements StudentService {

	@Autowired
	StudentRepository studentRepository;

	@Override
	public Student addStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public Student updateStudent(Student student) {
		return studentRepository.save(student);
	}

	@Override
	public Student getAll(int studentId) {
		return studentRepository.getOne(studentId);
	}

	@Override
	public void deleteStudent(int studentId) {
		studentRepository.deleteById(studentId);

	}

	@Override
	public List<Student> listAll() {
		return studentRepository.findAll();
	}

}
