package com.hcl.StudentCourseManyToManyProject.service;

import java.util.List;
import com.hcl.StudentCourseManyToManyProject.model.Course;

public interface CourseService {

	Course createCourse(Course course);
    Course updateCourse(Course course);
    void deleteCourse(int courseId);
    Course getCourse(int courseId);
    List<Course> getCourse();

}
