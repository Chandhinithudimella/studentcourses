package com.hcl.StudentCourseManyToManyProject.service;

import java.util.List;
import com.hcl.StudentCourseManyToManyProject.model.Student;

public interface StudentService {

	Student addStudent(Student student);

	Student updateStudent(Student student);

	Student getAll(int studentId);

	void deleteStudent(int studentId);

	List<Student> listAll();

}
