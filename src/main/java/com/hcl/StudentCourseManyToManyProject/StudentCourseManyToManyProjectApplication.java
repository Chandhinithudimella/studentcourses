package com.hcl.StudentCourseManyToManyProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentCourseManyToManyProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentCourseManyToManyProjectApplication.class, args);
	}

}
