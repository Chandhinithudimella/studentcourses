package com.hcl.StudentCourseManyToManyProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.StudentCourseManyToManyProject.model.Course;
import com.hcl.StudentCourseManyToManyProject.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {

	

}
