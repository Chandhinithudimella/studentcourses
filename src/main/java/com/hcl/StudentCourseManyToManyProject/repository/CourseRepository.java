package com.hcl.StudentCourseManyToManyProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.StudentCourseManyToManyProject.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course,Integer> {

	

	

}
