package com.hcl.StudentCourseManyToManyProject.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.StudentCourseManyToManyProject.model.Course;
import com.hcl.StudentCourseManyToManyProject.service.CourseService;

@RestController
public class CourseController {

	@Autowired
	CourseService courseService;

	@PostMapping("/course")
	private Course createCourse(@RequestBody Course course) {
		return courseService.createCourse(course);
	}

	@PutMapping("/course")
	private Course updateCourse(@RequestBody Course course) {
		return courseService.updateCourse(course);
	}

	@GetMapping("/course/{courseId}")
	private Course getCourseDetails(@PathVariable("courseId") int courseId) {
		return courseService.getCourse(courseId);
	}

	@GetMapping("/course")
	private List<Course> getAllCourseDetails() {
		return courseService.getCourse();
	}

	@DeleteMapping("/course/{courseId}")
	private void deleteCourse(@PathVariable("courseId") int courseId) {
		courseService.deleteCourse(courseId);
	}

}
