package com.hcl.StudentCourseManyToManyProject.controller;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.StudentCourseManyToManyProject.model.Student;
import com.hcl.StudentCourseManyToManyProject.service.StudentService;

@RestController
public class StudentController  {
	
	@Autowired
	StudentService studentService;
	
	@PostMapping("/student")
	private Student addStudent(@RequestBody Student student) {
		return studentService.addStudent(student);
	}
	
	@PutMapping("/student")
    private Student updateStudent(@RequestBody Student student) {
		return studentService.updateStudent(student);
	}
	
	@GetMapping("/student/{studentId}")
	private Student getAllStudents(@PathVariable("studentId") int studentId) {
		return studentService.getAll(studentId);
	}
	
	@DeleteMapping("/student/{studentId}")
	private void deleteStudent(@PathVariable("studentId") int studentId) {
		studentService.deleteStudent(studentId);
	}
	@GetMapping("/studentlist")
	public List<Student> listAll() {
		return studentService.listAll();
	}

}
